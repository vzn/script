#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests, \
    json, \
    time, \
    traceback, \
    threading

from concurrent.futures import ThreadPoolExecutor

# cookie 字符串  直接拷贝过来
v = 'your cookie ..... '

tmp = v.split(';')

cookie = {}
# 自动设置 cookie 键值对
for i in tmp:
    k, v = i.split('=', 1)
    cookie[k] = v

# 请求头字符串 可以不用
# s = '''Host: www.google.com
# User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:54.0) Gecko/20100101 Firefox/54.0
# Accept: */*
# Accept-Language: zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3
# Accept-Encoding: gzip, deflate, br
# Content-Type: application/x-www-form-urlencoded;charset=utf-8
# Referer: https://www.google.com/voice/b/1
# Content-Length: 108
# Connection: keep-alive
# '''


# 请求头
header = {}
header['Host'] = 'www.google.com'
header['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:54.0) Gecko/20100101 Firefox/54.0'
header['Accept'] = '*/*'
header['Accept-Language'] = 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3'
header['Accept-Encoding'] = 'gzip, deflate, br'
header['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'
header['Referer'] = 'https://www.google.com/voice/b/1'
header['Content-Length'] = '108'
header['Connection'] = 'keep-alive'
header['Pragma'] = 'no-cache'
header['Cache-Control'] = 'no-cache'

# 号码参数
data = {'sid': '3',
        'mid': '6',
        'req': '[null,"+1123456789",true,""]',
        '_rnr_se': 'xxxxxxxxxxxxxx'
        }

url = 'https://www.google.com/voice/b/1/service/post'

file_path = 'log/%s.log' % time.strftime("%Y-%m-%d_%X")

f = open(file_path, 'w', encoding='utf8')


def log(n, status, msg, info=None):
    log = '{time}---\
           第 {n} 次---\
           状态码: {status}---\
           msg: {msg}---\
           info: {info} \n'.format(n=n,
                                   time=time.strftime("%Y-%m-%d %X"),
                                   status=status,
                                   msg=msg,
                                   info=info)
    f.write(log)
    if n % 100 == 0:
        f.flush()


n = 0


def run():
    global n
    while True:
        n += 1
        print('第 %s 次' % n)
        print('start...')
        r = requests.post(url=url, cookies=cookie, headers=header, data=data)
        try:
            print('状态码:', r.status_code)
            print('end...')
            # [[null, null, "There was an error with your request. Please try again."]]
            status = json.loads(r.text)
            # if status[0][0] != None and status[0][1] != None or status[0][2] != 'There was an error with your request. Please try again.' :
            print(status)
            if status[0][0] != None and status[0][1] != None:
                f.close()
                break
            log(n, r.status_code, r.text)
        except Exception as e:
            log(n, r.status_code, e, traceback.format_exc()+r.content)
            f.close()
            f.flush()
            print('可能抢到了....')
            break


p = ThreadPoolExecutor(3)  # 多线程
if __name__ == '__main__':
    for i in range(5):
        p.submit(run)