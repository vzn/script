import requests
from bs4 import BeautifulSoup

r1 = requests.get('https://github.com/login')
s1 = BeautifulSoup(r1.text,'html.parser')   # 解析html
token = s1.find(name='input',attrs={'name':'authenticity_token'}).get('value')  # get csrftoken
r1_cookie_dict = r1.cookies.get_dict()  # 获取当前页面的cookie

# post -> username password csrftoken
r2 = requests.post(
    url='https://github.com/session',
    data={'commit':'Sign+in',
          'utf8':'✓',
          'authenticity_token':token,
          'login':'xxxxx',  # username 
          'password':'xxxxxx'},  # password
    cookies=r1_cookie_dict  # server 可能提示 not cookies
)

r2_cookie_dict = r2.cookies.get_dict()  # 获取登录后的 cookies
cookie_dict = {}
# 合并 cookies
cookie_dict.update(r1_cookie_dict)
cookie_dict.update(r2_cookie_dict)

# 访问登录后的页面来验证有没有登录
r3 = requests.get(
    url='https://github.com/settings/emails',
    cookies= cookie_dict,)

print(r3.text)
