# LOGIN Github
- post登录之前先发送get请求来获取csrftoken
```python
token = s1.find(name='input',attrs={'name':'authenticity_token'}).get('value')
```
- post提交携带csrftoken
```python
data = {
    ...
    'authenticity_token':token,
    }
```
- 合并cookie
```python
r2_cookie_dict = r2.cookies.get_dict()  # 获取登录后的 cookies
cookie_dict = {}
# 合并 cookies
cookie_dict.update(r1_cookie_dict)
cookie_dict.update(r2_cookie_dict)
```
## 模块
- BeautifulSoup
- requests
## 使用
15:16line
```python
'login':'xxxxx',  # your username
'password':'xxxxxx'},  # your  password
```
