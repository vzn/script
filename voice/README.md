# Voice
## Config

- cookie

```python
v = 'your cookie'
```

- header

```python
header = {}    # your browser request header
```

- url

```
url = 'your post url'
```

- data

```python
# your data
data = {
        'sid': '3', 
        'mid': '6', 
        'req': '[null,"+11234567890",true,""]',    # your number
        '_rnr_se': 'XXXXXXXXXXXXXXX'}   # your _rnr_se
```


